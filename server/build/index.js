"use strict";

var _express = _interopRequireDefault(require("express"));

var _helmet = _interopRequireDefault(require("helmet"));

var _cors = _interopRequireDefault(require("cors"));

var _passport = _interopRequireDefault(require("passport"));

var _connection = _interopRequireDefault(require("./database/connection"));

var _google = _interopRequireDefault(require("./config/google.config"));

var _Auth = _interopRequireDefault(require("./API/Auth"));

var _Restaurant = _interopRequireDefault(require("./API/Restaurant"));

var _Food = _interopRequireDefault(require("./API/Food"));

var _Menu = _interopRequireDefault(require("./API/Menu"));

var _Image = _interopRequireDefault(require("./API/Image"));

var _Order = _interopRequireDefault(require("./API/Order"));

var _Review = _interopRequireDefault(require("./API/Review"));

var _User = _interopRequireDefault(require("./API/User"));

var _route = _interopRequireDefault(require("./config/route.config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require('dotenv').config();

//passport config
(0, _google["default"])(_passport["default"]);
(0, _route["default"])(_passport["default"]); // private route authentication config

var zomato = (0, _express["default"])();
zomato.use(_express["default"].json()); //to read json body request (middleware) 

zomato.use((0, _cors["default"])());
zomato.use((0, _helmet["default"])());
zomato.use(_passport["default"].initialize()); //Application Router

zomato.use("/auth", _Auth["default"]);
zomato.use("/restaurant", _Restaurant["default"]);
zomato.use("/food", _Food["default"]);
zomato.use("/menu", _Menu["default"]);
zomato.use("/image", _Image["default"]);
zomato.use("/user", _User["default"]);
zomato.use("/review", _Review["default"]);
zomato.use("/order", _Order["default"]);
zomato.listen(5000, function () {
  (0, _connection["default"])().then(function () {
    console.log("Server is running...");
  })["catch"](function (e) {
    console.log("Server is running, but database connection failed");
    console.log(e);
  });
});